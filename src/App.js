import React, {Component} from 'react';
import './App.css';
import Home from "./containers/home/home";
import {BrowserRouter, Route} from "react-router-dom";
import SignUpPage from "./containers/signUp/signUpPage";

class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Route path="/" exact component={Home} />
                <Route path="/salesperson/signup" exact component={SignUpPage} />
            </BrowserRouter>
        );
    }
}

export default App;
