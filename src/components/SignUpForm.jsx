import React, {Component} from "react";
import SuccessMessage from "./SuccessMessage";

class SignUpForm extends Component {
    state = {
        inDB: false,
        id: "",
        password: ""
    };

    checkID = () => {
        fetch("http://localhost:8080/salesperson/exist", {
            method: "POST",
            body: JSON.stringify(this.state)
        }).then(res => {
            if (res.status === 200) {
                this.setState({inDB: true})
            } else {
                throw new Error()
            }
        }).catch(() => this.setState({id: ""}))
    };

    handleCheckID = (e) => {
        e.preventDefault();
        this.checkID()
    };

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleSubmit = () => {
        fetch("http://localhost:8080/salesperson/sign-up", {
            method: "POST",
            body: JSON.stringify(this.state)
        }).then(res => {
            if (res.status === 200) {
                this.props.onComplete()
            }
        })
    };

    render() {
        const pass = () => {
            return (
                <React.Fragment>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password:</label>
                        <input type="password" className="form-control" value={this.state.password}
                               onChange={this.handleChange} name="password" disabled={this.props.status}/>
                    </div>
                    {
                        !this.props.status ?
                            <button onClick={this.handleSubmit} type="submit" className="btn btn-primary">
                                Sign Up</button>
                            :
                            <SuccessMessage message="Sign Up Completed"/>
                    }
                </React.Fragment>
            );
        };

        return (
            <React.Fragment>
                <h1>Sign Up Form Salesperson</h1>
                <form onSubmit={this.handleCheckID}>
                    <div className="form-group my-2">
                        <label htmlFor="exampleInputEmail1">Referral Code:</label>
                        <input className="form-control"
                               name="id"
                               value={this.state.id}
                               disabled={this.state.inDB}
                               onChange={this.handleChange}
                        />
                    </div>
                    {this.state.inDB ? pass() : <React.Fragment></React.Fragment>}
                </form>
            </React.Fragment>
        );
    }
}

export default SignUpForm;