import React, {Component} from "react";
import {Link} from "react-router-dom";

class Login extends Component {
    state = {
        username: "",
        id: "",
        password: "",
        error: false
    };

    handleSubmit = (e) => {
        e.preventDefault();
        fetch(this.props.link, {
            method: "POST",
            body: JSON.stringify(this.state)
        }).then((response => {
            if (response.status !== 200) {
                throw new Error()
            }
            response.json().then(result => {
                localStorage.setItem("token", result.token);
                this.props.onLogin()
            })
        })).catch(() => this.setState({error: true}));
    };

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    render() {
        let id = (
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Username</label>
                <input className="form-control" name="username" value={this.state.username}
                       onChange={this.handleChange} aria-describedby="emailHelp"/>
            </div>
        );
        if (this.props.role === "salesperson") {
            id = (
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Referral Code:</label>
                    <input className="form-control" name="id" value={this.state.id}
                           onChange={this.handleChange} aria-describedby="emailHelp"/>
                </div>
            )
        }
        return (
            <div className="card my-2">
                <div className="col-11">
                    <form onSubmit={this.handleSubmit} method="POST">
                        {id}
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" value={this.state.password}
                                   onChange={this.handleChange} name="password"/>
                        </div>
                        <button type="submit" className="btn btn-primary my-1">Login</button>
                    </form>
                    {
                        this.props.role === "salesperson" ?
                            <Link to="/salesperson/signup">
                                <button type="button" className="btn btn-warning my-2">Sign Up</button>
                            </Link>
                            :
                            <React.Fragment></React.Fragment>
                    }
                </div>
            </div>
        );
    }
}

export default Login;