import React, {Component} from "react";
import RolePopUp from "./rolePopUp";

class Body extends Component {
    state = {
        showPopUp: false
    };

    handleRolePopUpTrue = () => {
        this.setState({showPopUp: true});
    };

    handleRolePopUpFalse = () => {
        this.setState({showPopUp: false});
    };

    render() {
        return (
            <div>
                <div className="row col-6">
                    {/*<p>Your Token is: {localStorage.getItem("token")}</p>*/}
                </div>
                <button onClick={this.props.onLogOut} type="button" className="btn btn-danger">Logout</button>
                <button onClick={this.handleRolePopUpTrue} type="button" className="btn btn-info mx-2">Role</button>
                {!this.state.showPopUp ? <React.Fragment></React.Fragment> :
                    <RolePopUp onExit={this.handleRolePopUpFalse}/>}
            </div>
        );
    }
}

export default Body;