import React, {Component} from "react";
import RoleSelect from "./roleSelect";
import Login from "./login";

class Logon extends Component {
    state = {
        role: ""
    };

    handleChange = (role) => {
        this.setState({role});
    };

    render() {
        let login;
        if (this.state.role === "") {
            login = (
                <div></div>
            );
        } else if (this.state.role === "admin") {
            login = (
                <Login link="http://localhost:8080/admin/login" onLogin={this.props.onLogin} role={this.state.role} />
            );
        } else if (this.state.role === "salesperson") {
            login = (
                <Login link="http://localhost:8080/salesperson/login" role={this.state.role} onLogin={this.props.onLogin} />
            );
        }
        return (
            <React.Fragment>
                <RoleSelect onSelect={this.handleChange} />
                { login }
            </React.Fragment>
        );
    }
}

export default Logon;