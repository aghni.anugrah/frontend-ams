import React, {Component} from "react";

class RoleSelect extends Component {
    pickRole = () => {
        return (
            <select className="form-control" onChange={(e) => this.props.onSelect(e.target.value)} id="cars">
                <option value="">Select Role...</option>
                <option value="admin">Admin</option>
                <option value="salesperson">Salesperson</option>
            </select>
        )
    };

    render() {
        return this.pickRole()
    }
}

export default RoleSelect;