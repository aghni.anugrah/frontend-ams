import React, {Component} from "react";
import {Link} from "react-router-dom";

class SuccessMessage extends Component {
    render() {
        return (
            <div className="alert alert-success alert-dismissible fade show my-2" role="alert">
                <strong>Message:</strong>
                <p>{this.props.message}</p>
                <Link to="/">
                    <button className="close" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Link>
            </div>
        );
    }
}

export default SuccessMessage;