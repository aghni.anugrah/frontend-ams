import React, {Component} from "react";

class NavBar extends Component {
    render() {
        return (
            <React.Fragment>
                <nav className="navbar navbar-light bg-light">
                    <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </nav>
            </React.Fragment>
        );
    }
}

export default NavBar;