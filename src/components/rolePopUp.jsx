import React, {Component} from "react";

class RolePopUp extends Component {
    state = {
      message: ""
    };

    handleRole = () => {
        const token = "Bearer " + localStorage.getItem("token");
        fetch("http://localhost:8080/test-role", {
            method: "GET",
            headers: {
                'Authorization': token
            }
        }).then(res => res.json()).then(result => this.setState({message: result.message}))
    };

    handleClose = () => {
        this.setState({message: ""});
        this.props.onExit()
    };

    componentDidMount() {
        this.handleRole()
    }

    render() {
        let role;
        if (this.state.message === "") {
            role = <strong>Loading...</strong>
        } else {
            role = <strong>{this.state.message}</strong>
        }
        return (
            <div className="alert alert-success alert-dismissible fade show my-2" role="alert">
                {role}
                <button onClick={this.handleClose} type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        );
    }
}

export default RolePopUp;