import React, {Component} from "react";
import SignUpForm from "../../components/SignUpForm";

class SignUpPage extends Component {
    state = {
        signedUp: false
    };

    handleSignUp = () => {
        this.setState({signedUp: true})
    };

    render() {
        return (
            <SignUpForm onComplete={this.handleSignUp} status={this.state.signedUp}/>
        );
    }
}

export default SignUpPage;