import React, {Component} from "react";
import Body from "../../components/Body";
import Logon from "../../components/logon";

class Home extends Component {
    state = {
        loggedIn: false
    };

    constructor(props) {
        super(props);
        const token = localStorage.getItem("token");
        if (token !== null) {
            this.state.loggedIn = true
        }
    }

    handleLogIn = () => {
        this.setState({loggedIn: true})
    };

    handleLogOut = () => {
        localStorage.clear();
        this.setState({loggedIn: false})
    };

    render() {
        if (!this.state.loggedIn) {
            return (
                <React.Fragment>
                    <h1>AMS</h1>
                    <Logon onLogin={this.handleLogIn}/>
                </React.Fragment>
            );
        } else {
            return (
                <React.Fragment>
                    <h1>AMS</h1>
                    <Body onLogOut={this.handleLogOut} />
                </React.Fragment>
            );
        }
    }
}

export default Home;